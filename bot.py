from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram.types import ContentType
from keyboard import *

from config import TOKEN

bot = Bot(token=TOKEN)
dp = Dispatcher(bot)

cats = ['BAACAgIAAxkBAAIEuGN7q0PTCTpgrpv16TX-ehU8PtqzAAKZJgAC8A7YS1HJ9M2B6gnKKwQ',
        'BAACAgIAAxkBAAIEumN7q1z8HoG9sM0RXp1d4-C4EbsHAAKdJgAC8A7YS5dt1M4EXgIgKwQ',
        'BAACAgIAAxkBAAIEvGN7q2yw_bvTRqwUDBkxLhUYgmomAAKhJgAC8A7YS5SUhXtetzcuKwQ',
        'BAACAgIAAxkBAAIEvmN7q3uAQkP_6mu3W0EmqWXbmzAwAAKjJgAC8A7YS40Bjm495Eq3KwQ',
        'BAACAgIAAxkBAAIEwGN7q4pWa8H821pXUDig_tO7DZIHAAKlJgAC8A7YS55TeleouvQ4KwQ',]

animals = ['AgACAgIAAxkBAAIErGN7qtoak2th85qIRN0Ed5YYqwGvAAIFwzEb8A7YS73N6Dsw54N-AQADAgADeQADKwQ',
           'AgACAgIAAxkBAAIErmN7qu9Bmmsw2kij39JLSLj3KUprAAIGwzEb8A7YSw_w7RUeFUFXAQADAgADeQADKwQ',
           'AgACAgIAAxkBAAIEsGN7qv5Hty_fFZzZwLcbvUZgpjEEAAIHwzEb8A7YS3NXxmcdMT9pAQADAgADeQADKwQ',
           'AgACAgIAAxkBAAIEK2N7mnqAaOOKYp3p-Ee47z-Cwi5wAAIIwzEb8A7YS2zFxSKujBIGAQADAgADeQADKwQ',
           'AgACAgIAAxkBAAIELWN7moo0ToLPb69vJ62Tv1TgNfnJAAIJwzEb8A7YS0qg8Sk8ycU6AQADAgADeQADKwQ']
cars = ['AgACAgIAAxkBAAIEL2N7mwpKfP1Vj3vxFSvmgGNakFTxAAIPwzEb8A7YS2s9mGTnSOQQAQADAgADeQADKwQ',
        'AgACAgIAAxkBAAIEMWN7myaQatN1y3eojarw4DATTTsiAAIRwzEb8A7YS8nkN5ITV0QvAQADAgADeQADKwQ',
        'AgACAgIAAxkBAAIENWN7m0YIJHJPYGNAbJcQFwpKFXpwAAITwzEb8A7YS78DXpdEtLDvAQADAgADeQADKwQ',
        'AgACAgIAAxkBAAIEN2N7m1rCnH026XD1xYBqoMlx039wAAIUwzEb8A7YS6Z51Bht_86rAQADAgADeQADKwQ',
        'AgACAgIAAxkBAAIEOWN7m2qwC6ZgArM4WZ8qpOJeJFiLAAIVwzEb8A7YS5KC_flA_O7UAQADAgADeQADKwQ',]
people = ['AgACAgIAAxkBAAIHXWN86cyyfijlFZyOL_1HK5hBPErFAAJ7wDEbX9HoS4mRtUtLTfOWAQADAgADeQADKwQ',
          'AgACAgIAAxkBAAIHXmN86cxc6EBw5x2qHkaXormtjL1oAAJ8wDEbX9HoS510WYWbhod3AQADAgADeQADKwQ',
          'AgACAgIAAxkBAAIHYGN86cyvck9HhHzstsR5c0w2B08YAAJ-wDEbX9HoS4mM8ocbrC6wAQADAgADeQADKwQ',
          'AgACAgIAAxkBAAIHX2N86cw1VI69xQnlT-Zmx1OQsDN5AAJ9wDEbX9HoS1UvGo4kvwAB2AEAAwIAA3kAAysE',
          'AgACAgIAAxkBAAIHYWN86czFXK4yUnk2ijn2IV_HHR2AAAJ_wDEbX9HoS8h8QYTYKebgAQADAgADeQADKwQ']

joke = ['BAACAgIAAxkBAAIGYGN84YYmjoZVQuoZ_wOschXoV8arAAKMIAACX9HoS_onGaG4ZCRNKwQ',
        'BAACAgIAAxkBAAIGYmN84bShYvDdayMHZ0GST_FwAxbjAAKOIAACX9HoS-4aPougMb21KwQ',
        'BAACAgIAAxkBAAIGZGN84dq-E8cDMehNWnx186XDeLJSAAKPIAACX9HoS-sbAyDq4hGhKwQ',
        'BAACAgIAAxkBAAIGZmN84gNiMOZh0zBlTrT9Jky0DIPwAAKQIAACX9HoS18Qkkgh6HmVKwQ',
        'BAACAgIAAxkBAAIGaGN84izv02bB7pcIHgsp7BVT6yMHAAKRIAACX9HoSyXrGQMVGhKWKwQ',]

tiktok = ['BAACAgIAAxkBAAIGcGN85Wl_HmVWrsflP1_cpchAeSpcAAKeIAACX9HoS652YpeAaqv3KwQ',
          'BAACAgIAAxkBAAIGc2N85cTq6g-Cg2sR6tGcpwZPosV5AAKgIAACX9HoS7DLGmTbUPb0KwQ',
          'BAACAgIAAxkBAAIGe2N85gUPAAGKHJQSPDOloYVI9M3ScAACpCAAAl_R6EuVAYmBEuNXNCsE',
          'BAACAgIAAxkBAAIG7WN85nyCZrHLzgS1qFtUx2PkZ-aMAAKuIAACX9HoS3V59Td79d_WKwQ',
          'BAACAgIAAxkBAAIG-mN85qscwBrJgZKepxVrMvIf4lTtAAKxIAACX9HoS_iG6VGCI27rKwQ']

@dp.message_handler(content_types=ContentType.VIDEO)
async def echo_video(message: types.Message):
    await message.reply(message.video.file_id)


@dp.message_handler(content_types=ContentType.PHOTO)
async def echo_photo(message: types.Message):
    await message.reply(message.photo[-1].file_id)

@dp.message_handler(commands=['start'])
async def process_start_command(message: types.Message):
    await message.reply('Привет!\nИспользуй /help, '
                        'чтобы узнать список доступных команд!')


@dp.message_handler(commands=['help'])
async def process_help_command(message: types.Message):
    msg = 'Я могу ответить на следующие команды:,\n/video,\n/photo'
    await message.reply(msg)

@dp.message_handler(text='/photo')
async def send_photo(message: types.Message):
    await message.reply('Фото на выбор!', reply_markup=greet_kb1)


@dp.message_handler(text='/video')
async def send_video(message: types.Message):
    await message.reply('Видео на выбор!', reply_markup=greet_kb2)

@dp.message_handler()
async def echo_photo(message: types.Message):
    album = types.MediaGroup()
    if message.text == 'Машины':
        for x in range(0, 5):
            album.attach_photo(photo=cars[x], caption='Спортивные машины мира')
        await message.answer_media_group(media=album)
    elif message.text == 'Люди':
        for x in range(0, 5):
            album.attach_photo(photo=people[x], caption='ФотоСет 202-')
        await message.answer_media_group(media=album)
    elif message.text == 'Животные':
        for x in range(0, 5):
            album.attach_photo(photo=animals[x], caption='Мир животных!')
        await message.answer_media_group(media=album)
    elif message.text == 'Коты':
        for x in range(0, 5):
            album.attach_video(video=cats[x], caption="Смешные видео с участием котов")
        await message.answer_media_group(media=album)
    elif message.text == "Юмор":
        for x in range(0,5):
            album.attach_video(video=joke[x], caption='Юмористические видео на разные темы')
        await message.answer_media_group(media=album)
    elif message.text == "TikTok":
        for x in range(0,5):
            album.attach_video(video=tiktok[x], caption='Красивые места нашей Планеты')
            await message.answer_media_group(media=album)

if __name__ == '__main__':
    executor.start_polling(dp)